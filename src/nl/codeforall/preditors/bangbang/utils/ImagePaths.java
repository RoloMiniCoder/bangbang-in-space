package nl.codeforall.preditors.bangbang.utils;

public class ImagePaths {

    public static final String INTRO_TEXT = "resources/startmenu.png";
    public static final String INSTRUCTIONS = "resources/instructions.png";
    public static final String BACKGROUND = "resources/background.jpg";
    public static final String GAME_OVER = "resources/gameover.png";
    public static final String PLAYER_SHIP = "resources/player.png";
    public static final String BULLET = "resources/laser.png";

    public static final String EXPLOSION = "resources/explosion.png";
    public static final String ENEMY_RED = "resources/enemy-red.png";
    public static final String ENEMY_BLUE = "resources/enemy-blue.png";
    public static final String ENEMY_CIRCLE = "resources/enemy-circle.png";
}
