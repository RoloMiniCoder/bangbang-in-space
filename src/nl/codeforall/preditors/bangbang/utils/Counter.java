package nl.codeforall.preditors.bangbang.utils;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Text;

public class Counter {
    private static final String BASE_TEXT = "Your score - ";

    private Text text;
    private int value;
    private int killedEnemies;

    public Counter(int x, int y){
        text = new Text(x, y,BASE_TEXT + value);
        text.setColor(Color.GREEN);
    }

    public void add(int amount){
        killedEnemies++;
        value += amount;
        text.setText(BASE_TEXT + value);
        text.setColor(Color.GREEN);
        text.draw();
    }

    public void reset(){
        killedEnemies = 0;
        value = 0;
        text.setText(BASE_TEXT + value);
    }

    public void show(){
        text.draw();
    }

    public void hide(){
        text.delete();
    }

    public int getScore(){
        return value;
    }

    public int getKilledEnemies(){
        return killedEnemies;
    }
}
