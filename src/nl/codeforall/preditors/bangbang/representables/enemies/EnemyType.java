package nl.codeforall.preditors.bangbang.representables.enemies;

import nl.codeforall.preditors.bangbang.utils.ImagePaths;

enum EnemyType {
        RED(ImagePaths.ENEMY_RED, 1, 3, 1),
        BLUE(ImagePaths.ENEMY_BLUE,2, 3, 2),
        CIRCLE(ImagePaths.ENEMY_CIRCLE,3, 3, 3);

        final String PATH_TO_PICTURE;
        final int SPEED;
        final int HEALTH;
        final int POINTS;

        EnemyType(String pathToPicture, int speed, int health, int points) {
            PATH_TO_PICTURE = pathToPicture;
            SPEED = speed;
            HEALTH = health;
            POINTS = points;
        }

        static EnemyType getRandom(){
            return values()[(int)(Math.random() * values().length)];
        }
}
