package nl.codeforall.preditors.bangbang.representables.enemies;

import nl.codeforall.preditors.bangbang.phases.Game;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class EnemyPool {
    private static final int SIZE_TO_GROW = 20;

    private Queue<Enemy> pool;

    public EnemyPool() {
        pool = new LinkedList<>();
        increasePoolSize();
    }

    public List<Enemy> getList(int amount) {
        List<Enemy> toReturn = new LinkedList<>();
        for (int i = 0; i < amount; i++) {
            toReturn.add(getEnemy());
        }
        return toReturn;
    }

    public Enemy getEnemy() {
        if (pool.size() == 0) {
            increasePoolSize();
        }
        Enemy e = pool.poll();
        e.show();
        return e;
    }

    public void recycle(Enemy e) {
        e.hide();
        e.reset(generateRandomValidX(e), -e.getHeight());
        pool.offer(e);
    }

    private void increasePoolSize() {
        for (int i = 0; i < SIZE_TO_GROW; i++) {
            Enemy e = new Enemy(EnemyType.getRandom());
            e.reset(generateRandomValidX(e), -e.getHeight());
            pool.add(e);
        }
    }

    private int generateRandomValidX(Enemy e) {
        return (int) ((Math.random() * ((Game.RIGHT_LIMIT - e.getWidth()) - Game.LEFT_LIMIT)) + Game.LEFT_LIMIT);
    }
}
