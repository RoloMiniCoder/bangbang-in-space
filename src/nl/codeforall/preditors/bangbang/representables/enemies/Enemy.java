package nl.codeforall.preditors.bangbang.representables.enemies;

import nl.codeforall.preditors.bangbang.representables.Sprite;
import nl.codeforall.preditors.bangbang.utils.ImagePaths;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Enemy extends Sprite {
    private final static int TIME_TO_EXPLODE = 15;

    private final Picture explosion = new Picture(0,0, ImagePaths.EXPLOSION);
    private final EnemyType type;

    private int health;
    private int timeExploding;

    Enemy(EnemyType type) {
        super(0, 0, type.PATH_TO_PICTURE, Direction.DOWN, type.SPEED);
        this.type = type;
        health = type.HEALTH;
        timeExploding = 0;
    }

    public void reset(int x, int y){
        explosion.delete();
        destroyed = false;
        health = type.HEALTH;
        timeExploding = 0;
        int deltaX = x-representation.getX();
        int deltaY = y-representation.getY();
        representation.translate(deltaX,deltaY);
    }

    @Override
    public void hit() {
        health--;
        if(health<=0){
            super.hit();
            explosion.translate(representation.getX() - explosion.getX(), representation.getY() - explosion.getY());
            explosion.draw();
            representation.delete();
        }
    }

    @Override
    public boolean isInvalidMovement(int x, int maxX, int y, int maxY) {
        return super.isInvalidMovement(x, maxX, y, maxY) || timeExploding >= TIME_TO_EXPLODE;
    }

    @Override
    public void move() {
        if(destroyed){
            timeExploding++;
        }
        super.move();
    }

    public int getPoints(){
        return type.POINTS;
    }

    public int getWidth(){
        return representation.getWidth();
    }

    public int getHeight(){
        return representation.getHeight();
    }
}
