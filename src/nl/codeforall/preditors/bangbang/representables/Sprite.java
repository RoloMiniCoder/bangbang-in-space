package nl.codeforall.preditors.bangbang.representables;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Sprite {
    private final int SPEED;

    protected Picture representation;
    protected Direction direction;
    protected boolean destroyed;

    protected Sprite(int x, int y, String pathToPicture, Direction startDirection, int speed) {
        representation = new Picture(x, y, pathToPicture);
        direction = startDirection;
        SPEED = speed;
    }

    public boolean isDestroyed(){
        return destroyed;
    }

    public void hit(){
        destroyed = true;
    }

    public void show() {
        representation.draw();
    }

    public void hide() {
        representation.delete();
    }

    public boolean collidedWith(Sprite o) {
        return !o.isDestroyed() && !this.isDestroyed()
                && o.representation.getY() <= representation.getMaxY()
                && o.representation.getX() <= representation.getMaxX()
                && o.representation.getMaxX() >= representation.getX()
                && o.representation.getMaxY() >= representation.getY();
    }

    public boolean isInvalidMovement(int x, int maxX, int y, int maxY) {
        return representation.getX() - SPEED <= x && direction == Direction.LEFT
                || representation.getMaxX() + SPEED >= maxX && direction == Direction.RIGHT
                || representation.getY() - SPEED <= y && direction == Direction.UP
                || representation.getMaxY() + SPEED >= maxY && direction == Direction.DOWN;
    }

    public void move() {
        switch (direction) {
            case STILL:
                return;
            case UP:
                representation.translate(0, -SPEED);
                break;
            case DOWN:
                representation.translate(0, SPEED);
                break;
            case LEFT:
                representation.translate(-SPEED, 0);
                break;
            case RIGHT:
                representation.translate(SPEED, 0);
                break;
            default:
                throw new EnumConstantNotPresentException(Direction.class, direction.name());
        }
    }

    protected enum Direction {
        UP,
        DOWN,
        STILL,
        LEFT,
        RIGHT
    }
}
