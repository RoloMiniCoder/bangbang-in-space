package nl.codeforall.preditors.bangbang.representables;

import nl.codeforall.preditors.bangbang.utils.ImagePaths;

import static nl.codeforall.preditors.bangbang.representables.Sprite.Direction.*;

public class Player extends Sprite {
    private boolean firing;

    public Player(int x, int y) {
        super(x, y, ImagePaths.PLAYER_SHIP, Direction.STILL, 3);
    }

    public Sprite shoot() {
        int middleX = (representation.getMaxX() + representation.getX()) / 2;
        Sprite b = new Sprite(middleX, representation.getY(), ImagePaths.BULLET, UP, 3);
        b.show();
        return b;
    }

    public void startFiring() {
        firing = true;
    }

    public void stopFiring() {
        firing = false;
    }

    public void setDirectionUp() {
        direction = UP;
    }

    public void setDirectionDown() {
        direction = DOWN;
    }

    public void setDirectionRight() {
        direction = RIGHT;
    }

    public void setDirectionLeft() {
        direction = LEFT;
    }

    public void setDirectionStill() {
        direction = STILL;
    }

    public boolean isFiring() {
        return firing;
    }

}
