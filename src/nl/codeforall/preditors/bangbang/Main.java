package nl.codeforall.preditors.bangbang;

import nl.codeforall.preditors.bangbang.keyboard.KeyboardManager;
import nl.codeforall.preditors.bangbang.phases.Game;
import nl.codeforall.preditors.bangbang.phases.Intro;
import nl.codeforall.preditors.bangbang.phases.Outro;
import nl.codeforall.preditors.bangbang.representables.enemies.EnemyPool;
import nl.codeforall.preditors.bangbang.utils.Counter;
import org.academiadecodigo.bootcamp.Sound;


public class Main {
    public static void main(String[] args) {
        KeyboardManager kbManager = new KeyboardManager();
        EnemyPool pool = new EnemyPool();
        Counter counter = new Counter(Game.LEFT_LIMIT, Game.TOP_LIMIT);
        Intro intro = new Intro();
        Game game = new Game(pool, counter);
        Outro outro = new Outro(counter);

        Sound music = new Sound("/resources/music.au");
        music.play(true);

        try {
            kbManager.setHandler(intro);
            intro.run();

            while (true) {
                kbManager.setHandler(game);
                game.run();
                kbManager.setHandler(outro);
                outro.run();
            }
        } catch (InterruptedException ex){
            ex.printStackTrace();
        }
    }
}
