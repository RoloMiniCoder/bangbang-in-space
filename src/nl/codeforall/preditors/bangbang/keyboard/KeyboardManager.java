package nl.codeforall.preditors.bangbang.keyboard;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class KeyboardManager implements KeyboardHandler {
    private Handler handler;
    private Keyboard kb;

    public KeyboardManager(){
        kb  = new Keyboard(this);
        initKeyboard();
    }

    private void initKeyboard(){
        for (Key k: Key.values()) {
            KeyboardEvent event = new KeyboardEvent();
            event.setKey(k.keyCode);
            event.setKeyboardEventType(k.type);
            kb.addEventListener(event);
        }
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        handler.keyPressed(keyboardEvent);
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        handler.keyReleased(keyboardEvent);
    }
}
