package nl.codeforall.preditors.bangbang.keyboard;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;

public interface Handler {
    void keyPressed(KeyboardEvent event);
    void keyReleased(KeyboardEvent event);
}
