package nl.codeforall.preditors.bangbang.keyboard;

import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;

public enum Key {
    SPACE_PRESS(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_SPACE),
    SPACE_RELEASE(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_SPACE),
    UP_PRESS(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_UP),
    UP_RELEASE(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_UP),
    DOWN_PRESS(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_DOWN),
    DOWN_RELEASE(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_DOWN),
    LEFT_PRESS(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_LEFT),
    LEFT_RELEASE(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_LEFT),
    RIGHT_PRESS(KeyboardEventType.KEY_PRESSED, KeyboardEvent.KEY_RIGHT),
    RIGHT_RELEASE(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_RIGHT),
    I(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_I),
    R(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_R),
    Q(KeyboardEventType.KEY_RELEASED, KeyboardEvent.KEY_Q);

    public final KeyboardEventType type;
    public final int keyCode;

    Key(KeyboardEventType type, int keyCode){
        this.type = type;
        this.keyCode = keyCode;
    }
}
