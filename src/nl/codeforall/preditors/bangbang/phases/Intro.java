package nl.codeforall.preditors.bangbang.phases;

import nl.codeforall.preditors.bangbang.keyboard.Handler;
import nl.codeforall.preditors.bangbang.utils.ImagePaths;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Intro implements Handler {
    private final Picture background;
    private final Picture intro;
    private final Picture instructions;

    private boolean showingInstructions;
    private boolean idling;

    public Intro() {
        background = new Picture(Game.LEFT_LIMIT, Game.TOP_LIMIT, ImagePaths.BACKGROUND);
        intro = new Picture(Game.LEFT_LIMIT, Game.TOP_LIMIT, ImagePaths.INTRO_TEXT);
        instructions = new Picture(Game.LEFT_LIMIT, Game.TOP_LIMIT, ImagePaths.INSTRUCTIONS);
    }

    public synchronized void run() throws InterruptedException {
        idling = true;
        background.draw();
        intro.draw();
        while (idling) {
            wait();
        }
        cleanup();
    }

    private void toggleInstructions() {
        Picture hide = showingInstructions ? instructions : intro;
        Picture show = showingInstructions ? intro : instructions;

        hide.delete();
        show.draw();
        showingInstructions = !showingInstructions;
    }

    private void cleanup() {
        background.delete();
        instructions.delete();
        intro.delete();
    }

    @Override
    public void keyPressed(KeyboardEvent event) {}

    @Override
    public void keyReleased(KeyboardEvent event) {
        switch (event.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                synchronized (this) {
                    idling = false;
                    notifyAll();
                }
                break;
            case KeyboardEvent.KEY_I:
                toggleInstructions();
                break;
            case KeyboardEvent.KEY_Q:
                System.exit(0);
        }
    }

}
