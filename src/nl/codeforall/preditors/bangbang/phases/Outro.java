package nl.codeforall.preditors.bangbang.phases;

import nl.codeforall.preditors.bangbang.keyboard.Handler;
import nl.codeforall.preditors.bangbang.utils.Counter;
import nl.codeforall.preditors.bangbang.utils.ImagePaths;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Outro implements Handler {
    private static final String MESSAGE_FIRST_HALF = "You killed ";
    private static final String MESSAGE_SECOND_HALF = " enemies and you score was ";

    private final Counter counter;
    private final Picture background;
    private final Text scoreMessage;

    private boolean idling;

    public Outro(Counter counter) {
        this.counter = counter;
        background = new Picture(Game.LEFT_LIMIT, Game.TOP_LIMIT, ImagePaths.GAME_OVER);
        scoreMessage = new Text(Game.LEFT_LIMIT, Game.TOP_LIMIT, MESSAGE_FIRST_HALF);
        scoreMessage.setColor(Color.GREEN);
    }

    synchronized public void run() throws InterruptedException {
        idling = true;
        background.draw();
        scoreMessage.setText(MESSAGE_FIRST_HALF + counter.getKilledEnemies()
                + MESSAGE_SECOND_HALF + counter.getScore());
        scoreMessage.draw();
        while (idling) {
            wait();
        }
        cleanup();
    }

    private void cleanup() {
        background.delete();
        scoreMessage.delete();
        idling = true;
    }

    @Override
    public void keyPressed(KeyboardEvent event) {
    }

    @Override
    public void keyReleased(KeyboardEvent event) {
        switch (event.getKey()) {
            case KeyboardEvent.KEY_R:
                synchronized (this) {
                    idling = false;
                    notifyAll();
                }
                break;
            case KeyboardEvent.KEY_Q:
                System.exit(0);
        }
    }
}
