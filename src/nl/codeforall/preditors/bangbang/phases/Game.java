package nl.codeforall.preditors.bangbang.phases;

import nl.codeforall.preditors.bangbang.keyboard.Handler;
import nl.codeforall.preditors.bangbang.representables.Sprite;
import nl.codeforall.preditors.bangbang.utils.Counter;
import nl.codeforall.preditors.bangbang.representables.enemies.Enemy;
import nl.codeforall.preditors.bangbang.representables.Player;
import nl.codeforall.preditors.bangbang.representables.enemies.EnemyPool;
import nl.codeforall.preditors.bangbang.utils.ImagePaths;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Game implements Handler {
    public static final int LEFT_LIMIT = 10;
    public static final int RIGHT_LIMIT = 730;
    public static final int TOP_LIMIT = 0;
    public static final int BOTTOM_LIMIT = 750;

    private static final int DELAY = 10;
    private static final int ENEMIES_PER_WAVE = 10;

    private final Counter counter;
    private final EnemyPool enemyPool;
    private final Picture background;

    private Player player;
    private List<Enemy> enemies;
    private List<Sprite> bullets;
    private boolean gameOver;

    public Game(EnemyPool enemyPool, Counter counter) {
        this.counter = counter;
        this.enemyPool = enemyPool;
        gameOver = false;
        background = new Picture(LEFT_LIMIT, TOP_LIMIT, ImagePaths.BACKGROUND);
    }

    public void run() throws InterruptedException {
        background.draw();
        enemies = enemyPool.getList(ENEMIES_PER_WAVE);
        bullets = new LinkedList<>();
        player = new Player(RIGHT_LIMIT / 2, BOTTOM_LIMIT - 50);
        player.show();
        counter.reset();
        counter.show();

        while (!gameOver) {
            Thread.sleep(DELAY);

            playerActs();
            moveBullets();
            moveEnemies();

            if (player.isDestroyed()) {
                gameOver = true;
            }
            if (enemies.size() == 0) {
                enemies = enemyPool.getList(ENEMIES_PER_WAVE);
            }
        }

        cleanup();
    }

    private void playerActs() {
        if (player.isInvalidMovement(LEFT_LIMIT, RIGHT_LIMIT, TOP_LIMIT, BOTTOM_LIMIT)) {
            return;
        }
        if (player.isFiring()) {
            bullets.add(player.shoot());
            player.stopFiring();
        }
        player.move();
    }

    private void moveBullets() {
        Iterator<Sprite> it = bullets.iterator();
        while (it.hasNext()) {
            Sprite b = it.next();
            if (b.isInvalidMovement(LEFT_LIMIT, RIGHT_LIMIT, TOP_LIMIT, BOTTOM_LIMIT)
                    || b.isDestroyed()) {
                b.hide();
                it.remove();
                continue;
            }
            b.move();
        }
    }

    private void moveEnemies() {
        Iterator<Enemy> it = enemies.iterator();
        while (it.hasNext()) {
            Enemy e = it.next();
            e.move();
            if (e.collidedWith(player)) {
                player.hit();
            }
            for (Sprite b : bullets) {
                if (e.collidedWith(b)) {
                    e.hit();
                    b.hit();
                    if (e.isDestroyed()) {
                        counter.add(e.getPoints());
                    }
                }
            }
            if (e.isInvalidMovement(LEFT_LIMIT, RIGHT_LIMIT, TOP_LIMIT, BOTTOM_LIMIT)) {
                it.remove();
                enemyPool.recycle(e);
            }
        }
    }

    private void cleanup() {
        background.delete();
        counter.hide();
        player.hide();
        gameOver = false;
        for (Enemy e : enemies) {
            enemyPool.recycle(e);
        }
        for (Sprite b : bullets) {
            b.hide();
        }
    }

    @Override
    public void keyPressed(KeyboardEvent event) {
        switch (event.getKey()) {
            case KeyboardEvent.KEY_UP:
                player.setDirectionUp();
                break;
            case KeyboardEvent.KEY_DOWN:
                player.setDirectionDown();
                break;
            case KeyboardEvent.KEY_LEFT:
                player.setDirectionLeft();
                break;
            case KeyboardEvent.KEY_RIGHT:
                player.setDirectionRight();
                break;
            case KeyboardEvent.KEY_SPACE:
                player.startFiring();
                break;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent event) {
        if (isDirection(event)) {
            player.setDirectionStill();
            return;
        }
        switch (event.getKey()) {
            case KeyboardEvent.KEY_SPACE:
                player.stopFiring();
                break;
            case KeyboardEvent.KEY_Q:
                System.exit(0);
        }
    }

    private boolean isDirection(KeyboardEvent event) {
        int keyCode = event.getKey();
        return keyCode == KeyboardEvent.KEY_DOWN
                || keyCode == KeyboardEvent.KEY_UP
                || keyCode == KeyboardEvent.KEY_LEFT
                || keyCode == KeyboardEvent.KEY_RIGHT;
    }

}
