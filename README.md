# BANG-BANG !
##### ...in space

This is a complete from scrath refactoring from the game [Bang-bang in space][bangbang-gitlab], from [Gijs][gijs-gitlab], [Jerrol][jerrol-gitlab] and [Matt][matt-gitlab].

Made as an instructional example to showcase some different strategies, thoughts and approaches at the same problem/behaviour.

[bangbang-gitlab]:<https://github.com/Noble3some/game.git>
[gijs-gitlab]: <https://gitlab.com/dominiouz>
[jerrol-gitlab]: <https://gitlab.com/jscambiel>
[matt-gitlab]:<https://gitlab.com/mattlcfa>